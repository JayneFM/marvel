const marvel = {
    render: () => {
        // Criacao da Hash para acesso a API
        const urlAPI = 'http://gateway.marvel.com/v1/public/characters?ts=1&apikey=76edc2da6cffef5aadecc84d6e4e6bed&hash=a4f327dd7684c77b2b5c73c28e7ec36d';
        const container = document.querySelector('#marvel-row');

        let contentHTML = '';

        fetch(urlAPI)
        // RESPOSTA DA REQUISICAO DA API
        .then(response => response.json())
        .then((json) => {
            console.log(json.data.results)
            for (const hero of json.data.results) {
                let urlHero = hero.urls[0].url;

                // Responsividade para mostrar os icones dos personagens
                // Crases servem para diferenciar o cod HTML do JS
                contentHTML += `
                <div class="col-6 col-md-4">
                    <a href="${urlHero}" target="_blank">
                        <img src="${hero.thumbnail.path}.${hero.thumbnail.extension}" alt="${hero.name}" class="img-thumbnail">
                    </a>
                    <h3 class="tittle">${hero.name}</h3>
                </div>`;
            }
            container.innerHTML = contentHTML;
        })
    }
};
marvel.render();