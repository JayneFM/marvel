# Projeto Marvel - @Author Jayne Ferreira <jayneferreira42610@gmail.com>

Este projeto foi gerado com  [Bootstrap](https://getbootstrap.com/docs/4.1/getting-started/introduction/) versão 4.1.3, [jQuery](https://blog.jquery.com/2018/01/20/jquery-3-3-1-fixed-dependencies-in-release-tag/) versão 3.3.1

## Screenshots

![App UI](/app.png)

# Projetos desenvolvidos

## Frontend com BootStrap
Para executar este projeto, apenas baixe a extensão [Live Server](https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer), na versão 5.6.1, na página "index.html", apertar com o botão direito, e clicar na opção "Open with Live Server", e aguardar para o carregamento da página no seu navegador padrão.
